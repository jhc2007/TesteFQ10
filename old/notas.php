<?php
include_once('connect_db.php');
include_once('header.php');
?>


 	<div class="container-fluid col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

        <div class="panel panel-primary">

            <div class="panel-heading text-center">
	            <h3 id="escola">Escola Básica e Secundária da Povoação</h3>
	            <h3 id="ebsp">EBSP</h3>
            </div>

            <div class="panel-body">

            	<div>
            		<h4>| Física e Química | 7ºC |</h4><br>
          		</div>
            	
            	<div>

<?php

	$result = $mysqli->query("SELECT ALUNO.`Numero`, ALUNO.`Nome`, FICHA.`Total` FROM ALUNO, FICHA WHERE ALUNO.`Numero` = FICHA.`Numero` AND FICHA.`Total` <> 'NULL';");
	$mysqli->dbError($result);

	while($row = $result->fetch_array(MYSQLI_NUM)) {
		printf("[%02d] %s => %d%%<br>", $row[0], ucwords(strtolower($row[1])), $row[2]);
		// echo $row[0]."\t".$row[1]."\t".$row[2]."<br>";
	}

	echo "</div><br>".$result->num_rows;

	$result->free();
	$mysqli->close();
?>
</div>
</div>
</div>
</body>
</html>