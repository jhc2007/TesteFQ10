<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if(!isset($_SESSION['turma']) || !isset($_SESSION['number'])) {
	header('Location: login.php');
   	exit();
} 

include_once('connect_db.php');
include_once('setup_parameters.php');

$turma = $_SESSION['turma'];
$number = $_SESSION['number'];

session_destroy();

$result = $mysqli->query("SELECT Nota FROM RESPOSTAS WHERE Turma = $turma AND Numero = $number;");
$mysqli->dbError($result);
$row = $result->fetch_array(MYSQLI_NUM);

if($row[0] == NULL) {
	$result->free_result();		
	$mysqli->close();
	header('Location: insert_classification.php');
	exit();
} 

$result->free_result();	
$result = $mysqli->query("SELECT Nome, Nota FROM ALUNO, RESPOSTAS WHERE ALUNO.`Turma` = RESPOSTAS.`Turma` AND ALUNO.`Numero` = RESPOSTAS.`Numero` AND ALUNO.`Turma`= $turma AND ALUNO.`Numero` = $number;");
$mysqli->dbError($result);

$row = $result->fetch_array(MYSQLI_NUM);

$result->close();
$mysqli->close();

//$_SESSION = array();
//session_unset();

include_once('header.php');
?>
		<br>
	 	<div class="container col-md-6 col-md-offset-3">
	        <div class="panel panel-primary">

	            <div class="panel-heading text-center">
		            <h3 id="escolanome"><?php echo $school_name; ?></h3>
		            <h3 id="escolasigla"><?php echo $school_initials; ?></h3>
	            </div>

	            <div class="panel-body">

	            	<div class="text-right">
	                	<h4>| Física e Química |</h4><br>
	              	</div>

					<div class="">
						<p>Nome:<?php echo " " . $row[0]; ?></p>
						<p>N&uacute;mero:<?php echo " " . $number; ?></p>
						<p>Classifica&ccedil;&atilde;o:<?php echo " " . $row[1] . "%"; ?></p>
					</div>

<!-- 					<div class="text-right">
						<a class="btn btn-primary" href="index.php">Voltar</a>
					</div> -->

	            </div>

	        </div>

	    </div>

	</body>
</html>