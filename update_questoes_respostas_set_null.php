<?php

   	include_once('questions.php');
   	include_once('connect_db.php');

   	$num_of_questions = sizeof($questions) - 1;

	$query = "UPDATE QUESTOES SET ";

	for ($i=1; $i < $num_of_questions; $i++) { 
		$query = $query . "Q" . $i . " = NULL, ";
	}
	$query = $query . "Q" . $i . " = NULL WHERE Turma = 'ct1'";

	$result = $mysqli->query($query);
	$mysqli->dbError($result);

	$query = "UPDATE RESPOSTAS SET ";

	for ($i=1; $i <= $num_of_questions; $i++) { 
		$query = $query . "R" . $i . " = NULL, ";
	}
	$query = $query . "Nota = NULL WHERE Turma = 'ct1'";
	
	$result = $mysqli->query($query);
	$mysqli->dbError($result);
	$mysqli->close();

	session_start();
	session_unset();
	session_destroy();


?>