<?php
	class My_mysqli extends mysqli {
		public function dbError($result) {
			if(!$result) {
				if(isset($_SESSION))
					session_destroy();
				header('Location: errormessage.php?e=1');
	    		exit();
			}
	    	// printf("Connect failed: %s\n", $mysqli->connect_error);
		}
		// public function dbDisconnect($result) {
		// 	// $result->free_result();
		// 	$result->close();
		// 	$this->close();
		// }
	}

	$mysqli = new My_mysqli("localhost", "root", "", "teste_fq");

	/* check connection */
	if ($mysqli->connect_errno) {
		header('Location: errormessage.php?e=1');
	    // printf("Connect failed: %s\n", $mysqli->connect_error);
	    exit();
	}
?>