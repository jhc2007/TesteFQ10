  <?php

    include_once('header.php');
    
    if(!isset($_SESSION))
      session_start();

    if(isset($_SESSION['number']))
      $number = $_SESSION['number'];

  ?>

    <div class="container-fluid col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <!-- <div class="row"> -->

        <!-- <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> -->

          <div class="panel panel-primary">

            <div class="panel-heading text-center">
              <h3 id="escola">Escola Básica e Secundária da Povoação</h3>
              <h3 id="ebsp">EBSP</h3>
            </div>

            <div class="panel-body">

<!--               <div class="text-right">
                <h4>| Física e Química | 7ºC |</h4><br>
              </div> -->

              <div>
                <h4>Password incorreta!</h4><br>
              </div>


              <form class="form-inline" action="delete_student.php" method="post">
                
                <div class="form-group">
                  <label for="inputNumber">Número</label>
                  <input type="number" min="1" max="21" class="form-control" style="width: 65px;" id="inputNumber" name="inputNumber" value="<?php echo $number;?>" required>
                </div>

                <button type="submit" class="btn btn-primary">Eliminar Registo</button>
              
              </form>
            </div>
          </div>
        <!-- </div> -->
      <!-- </div> -->
    </div>
  </body>
</html>