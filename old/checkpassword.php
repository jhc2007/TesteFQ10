  <?php
    include_once('header.php');
    if(!isset($_SESSION))
      session_start();

    if(isset($_SESSION['number']))
      $number = $_SESSION['number'];
    else
      $number = 1;
  ?>

    <div class="container-fluid col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <!-- <div class="row"> -->

        <!-- <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> -->

          <div class="panel panel-primary">

            <div class="panel-heading text-center">
              <h3 id="escola">Escola Básica e Secundária da Povoação</h3>
              <h3 id="ebsp">EBSP</h3>
            </div>

            <div class="panel-body">

              <div class="text-right">
                <h4>| Física e Química | 7ºC |</h4><br>
              </div>

              <form class="form-horizontal" action="check_password.php" method="post">
                
                <div class="form-group">
                  <label for="inputNumber" class="col-sm-3 control-label">Número</label>
                  <div class="col-sm-9">
                    <input type="number" style="width: 65px;" min="1" max="21" class="form-control" id="inputNumber" name="inputNumber" value="<?php echo $number;?>" required>
                  </div>
                </div>
     
                <div class="form-group">
                  <label for="inputPassword" class="col-sm-3 control-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-3 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary">Verificar</button>
                  </div>
                </div>
              
              </form>
            </div>
          </div>
        <!-- </div> -->
      <!-- </div> -->
    </div>
  </body>
</html>