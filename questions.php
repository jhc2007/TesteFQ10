<?php
//correct answer is always option1
//picture yes->1 no->0
$questions = array(
	array("question", 
	"option1",
	"option2",
	"option3",
	"option4",
	"picture"),
	//1
	array("Vários fios condutores de diferentes materiais encontram-se à mesma temperatura. Qual deles tem menor resistência?",
	"O que tiver maior secção reta, menor comprimento e do material com menor resistividade.",
	"O que tiver menor secção reta, menor comprimento e do material com menor resistividade.",
	"O que tiver maior secção reta, maior comprimento e do material com menor resistividade.",
	"O que tiver maior secção reta, menor comprimento e do material com maior resistividade.",
	0),
	//2
	array("Tendo em conta as medidas indicadas nos amperímetros e voltímetros intercalados em cada um dos circuitos, identifique qual das seguintes afirmações está correta:",
	"Numa associação de resistências em série, a corrente elétrica é igual em todas as resistências e a soma das d.d.p. entre os terminais de cada uma é igual à d.d.p. entre os terminais do gerador.",
	"Numa associação de resistências em série, a corrente elétrica é igual em todas as resistências e a d.d.p. entre os terminais de cada uma é igual à d.d.p. entre os terminais do gerador.",
	"Numa associação de resistências em paralelo, a corrente elétrica é igual em todas as resistências e a soma das d.d.p. nos extremos de cada uma é igual à d.d.p. entre os terminais do gerador.",
	"Numa associação de resistências em paralelo, a corrente elétrica no circuito principal é igual à soma das intensidades de corrente em cada resistência e a d.d.p. entre os terminais do gerador é igual à soma das d.d.p. nos terminais de cada uma das resistências.",
	1),
	//3
	array("No circuito esquematizado na figura, os três recetores são iguais e a corrente elétrica lida no amperímetro é I. Sendo R a resistência de cada um dos recetores, indique qual das seguintes afirmações está correta:",
	"A d.d.p. entre os terminais da associação dos três recetores e entre os terminais da pilha é (3RI).",
	"A d.d.p. entre os terminais de cada recetor e entre os terminais da pilha é (3RI).",
	"A d.d.p. entre os terminais de cada recetor é (RI)/3 e entre os terminais da pilha é (RI).",
	"A d.d.p. entre os terminais da associação dos três recetores é (RI) e entre os terminais da pilha é (3RI).",
	1),
	//4
	array("Uma pilha de força eletromotriz 9,0 V e resistência interna 500 m&ohm; fornece energia a um recetor, permitindo que no circuito se estabeleça uma corrente elétrica de intensidade 2,0 A. Qual é a d.d.p. entre os terminais da pilha quando o circuito está fechado?",
	"8,0 V",
	"9,0 V",
	"1,0 V ",
	"4,0 V",
	0),
	//5
	array("Sabendo que no circuito da figura o recetor de maior resistência dissipa energia à taxa de 15,7 J/s. Qual é a d.d.p. nos terminais da associação de resistências em paralelo, na resistência de 3 &ohm; e nos terminais do gerador, respetivamente?",
	"11,2 V; 12,6 V e 23,8 V",
	"22,4 V; 12,6 V e 35,0 V",
	"50,4 V; 12,6 V e 37,8 V",
	"11,2 V; 8,4 V e 19,6 V",
	1),
	//6
	array("Dois fios cilíndricos, um de cobre e outro de alumínio, possuem o mesmo comprimento e o mesmo diâmetro (a resistividade do alumínio é superior à do cobre). Se os dois fios forem percorridos pela mesma corrente elétrica, I, pode afirmar-se que:",
	"A tensão nos terminais do fio de cobre é menor do que nos terminais do fio de alumínio.",
	"As resistências dos dois fios são iguais.",
	"A resistência do fio de cobre é maior do que a resistência do fio de alumínio.",
	"A tensão nos terminais dos fios condutores é igual.",
	0),
	//7
	array("Considere um condutor óhmico puramente resistivo, com potência dissipada, P. Se a tensão aplicada nos seus terminais duplicar, o que acontece à potência por ele dissipada?",
	"Torna-se quatro vezes superior.",
	"Passa a ser quatro vezes menor. ",
	"Passa para metade.",
	"Torna-se duas vezes superior.",
	0),
	//8
	array("Um gerador de força eletromotriz 40 V e resistência interna 2 &ohm; está ligado em série a uma resistência. A corrente elétrica que percorre o circuito tem intensidade 5 A. Nestas condições, a potência elétrica transferida pelo gerado para o circuito elétrico é:",
	"150 W",
	"200 W",
	"50 W",
	"10 W",
	0),
	//9
	array("A curva característica de uma pilha é dada pela equação U = 3,0 - 0,50 I (SI). Que energia se dissipa na pilha, por segundo, quando está ligada a um circuito onde se estabelece uma corrente elétrica de 80 mA?",
	"3,2 mJ",
	"0,24 J",
	"0,040 J",
	"3,2 kJ",
	0),
	//10
	array("Tendo em conta os dados dos aparelhos de medida intercalados no circuito, indique qual das afirmações seguintes está correta.",
	"A quantidade de carga elétrica que atravessa a secção dos condutores deste circuito, em cada segundo, é de 1,5 C.",
	"A quantidade de carga elétrica que atravessa a secção dos condutores deste circuito, em cada segundo, é de 1,5 A.",
	"A energia elétrica transferida para o circuito, em cada segundo, é de 3 V.",
	"A energia elétrica transferida para o circuito, em cada segundo, é de 1,5 A.",
	1)
);
?>