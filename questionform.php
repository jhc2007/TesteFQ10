<?php
   
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

 include_once('questions.php');
 include_once('setup_parameters.php');

 if(isset($_SESSION['start_time'])) {
   //$start_time = $_SESSION['start_time'];
   //$now_time = time();
   if(time() - $_SESSION['start_time'] > $duration_s) {
      header('Location: errormessage.php?e=2');
      exit();
   }
 }


  if(isset($_SESSION['turma']) && isset($_SESSION['number']) && isset($_SESSION['question_numbers']) && isset($_SESSION['question_index'])) {
     // $number = $_SESSION['number'];

      $question_numbers = $_SESSION['question_numbers'];
      $question_index = $_SESSION['question_index'];
      
      if($question_index < sizeof($question_numbers)) {

        $question = $question_numbers[$question_index];
         //$question_index++;
         //$_SESSION['question_index'] = $question_index;
        
        include_once('header.php');
         //style='line-height: 1.6;'
         echo "<br>      
               <div class='container-fluid'>


                  <div class='panel panel-primary'>

                     <div class='panel-heading' style='font-size: 18pt; line-height: 1.6;'>" . ($question_index + 1) . ". " . $questions[$question][0] . "</div>

                     <div class='panel-body'>

               ";
      
         if($questions[$question][5]) {
            echo "<img class='img-responsive' src='png/q" . $question . ".PNG' class='w3-image'><br>";
         }
         
         echo "<form action='insert_answer.php' method='post' onsubmit='setRemainigTime()'>";

         echo "<input type='number' class='hidden' id='remainingtime' name='remainingtime'>";
         //echo "<input type='number' class='hidden' id='seconds' name='seconds'>";

         $options = array(1, 2, 3, 4);
         shuffle($options);
         
         foreach ($options as $option) {

            echo "<div class='container-fluid radio' style='font-size: 14pt;'>
                     <p><label><input type='radio' name='option' value='" . $option . "' required>" . $questions[$question][$option] . "</label></p>
                  </div>";
         }

         echo "<br>
               <div style='display: flex; justify-content: space-between;'>
               <button class='btn btn-primary' type='submit'>Seguinte</button>
               <div style='text-align: center; width: 80px; border: 1px solid #337ab7;'><h4 id='timer'></h4></div>
               </div>
               </form>
               </div>
               </div>
               </div>";


?>
         <script>
            var countDownDate =  <?php echo $_SESSION['client_end_time']; ?>;

            function setRemainigTime() {
              var now = new Date().getTime();
              document.getElementById("remainingtime").value = countDownDate - now;
            }
            //document.write(countDownDate);
            // Update the count down every 1 second
            var x = setInterval(function() {

              // Get today's date and time
              var now = new Date().getTime();
                
              // Find the distance between now and the count down date
              var distance = countDownDate - now;
                
              // Time calculations for days, hours, minutes and seconds
              //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
              //var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
              // Output the result in an element with id="demo"
              var text = minutes + ":";
              if(seconds < 10)
                text = text + "0";
              text = text + seconds;
              document.getElementById("timer").innerHTML = text;
                
              // If the count down is over, write some text 
              if (distance < 0) {
                clearInterval(x);
                document.getElementById("timer").style = "color: red;"
                document.getElementById("timer").innerHTML = "00:00";
              }
            }, 1000);

         </script>
        </body>
        </html>
<?php
      } else {
         header('Location: insert_classification.php');
         exit();   
      }
      
  } else {
    header('Location: login.php');
    exit();
  }
?>