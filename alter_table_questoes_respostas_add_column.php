<?php

	if (session_status() !== PHP_SESSION_ACTIVE) {
	    session_start();
	}
	session_unset();
	session_destroy();

	include_once('connect_db.php');


	for ($i=6; $i <= 10; $i++) { 
		$j = $i - 1;
		$query = "ALTER TABLE RESPOSTAS ADD R" . $i . " bit(1) AFTER R" . $j;
		$result =  $mysqli->query($query);
		$mysqli->dbError($result);
		//$result->free_result();

		$query = "ALTER TABLE QUESTOES ADD Q" . $i . " TINYINT(2) AFTER Q" . $j;
		$result =  $mysqli->query($query);
		$mysqli->dbError($result);
	}

	$mysqli->close();

	header('Location: index.php');
    exit();

?>