<?php
    
//session_start();
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

include_once('questions.php');
include_once('setup_parameters.php');

//if (session_status() == PHP_SESSION_NONE) {
//}

if(isset($_GET['t']) && isset($_GET['k'])) {
	$_SESSION['turma'] = '\'' .  $_GET['t'] . '\'';
	$_SESSION['chave'] = $_GET['k'];
}

include_once('header.php');
?>		<br>
	 	<!-- <div class="container-fluid pt-3 col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> -->
	 	<div class="container col-md-6 col-md-offset-3">
	        <div class="panel panel-primary">

	            <div class="panel-heading text-center">
		            <h3 id="escolanome"><?php echo $school_name; ?></h3>
		            <h3 id="escolasigla"><?php echo $school_initials; ?></h3>
	            </div>

	            <div class="panel-body">

	            	<div class="text-right">
	                	<h4>| Física e Química |</h4><br>
	              	</div>

					<div>
						<p><?php echo $test_content; ?></p>
						<p>N&uacute;mero de quest&otilde;es:<?php echo " " . (sizeof($questions) - 1); ?></p>
						<p>Dura&ccedil;&atilde;o:<?php echo " " . $duration_min . " min"; ?></p>
					</div>

 					<div class="text-right">

 						<form action='login.php' method='post' onsubmit='setClientTime()'>
 							<input type='number' class='hidden' id='clienttime' name='clienttime'>
 							<button class='btn btn-primary' type='submit'>Iniciar</button>
 						</form>
					</div>

	            </div>

	        </div>

	    </div>

	    <script type="text/javascript">

	    	function setClientTime() {
	    		var min = <?php echo $duration_min; ?>;
	    		//document.write(min);
	    		var date = new Date();
  				date.setMinutes(date.getMinutes() + min);
  				document.getElementById("clienttime").value = date.getTime();
	    	}

	    </script>

	</body>
</html>