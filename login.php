<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

	include_once('connect_db.php');
	include_once('setup_parameters.php');

	if(isset($_SESSION['turma']) && isset($_SESSION['chave'])) {
		$turma = $_SESSION['turma'];
		$key = $_SESSION['chave'];
	} else {
	   	header('Location: errormessage.php');
		exit();
	}

	if(!isset($_SESSION['client_end_time'])) {
		$_SESSION['client_end_time'] = $_POST['clienttime'];
	}
	
	//$password = '\'' .  $mysqli->real_escape_string($_POST['inputPassword']) . '\''; 


	$result = $mysqli->query("SELECT Numero FROM ALUNO WHERE Turma = $turma AND Chave = $key;");
	$mysqli->dbError($result);


	if($result->num_rows == 0) {

		$result->free_result();		
		$mysqli->close();
		header('Location: errormessage.php');
		exit();

	} 

	$number = $result->fetch_array(MYSQLI_BOTH)['Numero'];
	$result->free_result();

	$result = $mysqli->query("SELECT Nota FROM RESPOSTAS WHERE Turma = $turma AND Numero = $number");
	$mysqli->dbError($result);

	$respostas = $result->fetch_array(MYSQLI_NUM);
	$result->free_result();

	$_SESSION['turma'] = $turma;
	$_SESSION['chave'] = $key;
	$_SESSION['number'] = $number;

	if($respostas[0] != NULL) {
		header('Location: classification.php');
		$mysqli->close();
		exit();
	}

	$result = $mysqli->query("SELECT * FROM QUESTOES WHERE Turma = $turma AND Numero = $number");
	$mysqli->dbError($result);
	
	$question_numbers = $result->fetch_array(MYSQLI_NUM);
	$result->free_result();

	$question_numbers = array_slice($question_numbers, 2);

	$num_of_questions = sizeof($question_numbers);
	$question_index = 0;

	if($question_numbers[0] == NULL) {

		$question_numbers = range(1, $num_of_questions);

		//for ($i=1; $i <= $numquestions; $i++) { 
		//	array_push($questions_num, $i);
		//}


		shuffle($question_numbers);
		// array_unshift($question_numbers, 0);
/*		print_r($question_numbers);
		echo "<br>";*/

		$query = "UPDATE QUESTOES SET ";

		for ($i=1; $i < $num_of_questions; $i++) { 
			$query = $query . "Q" . $i . " = " . $question_numbers[$i - 1] . ", ";
		}
		$query = $query . "Q" . $i . " = " . $question_numbers[$i - 1] . " WHERE Turma = " . $turma . " AND Numero = " . $number;
		
		//echo "aqui<br>";
		//echo $query;
		//exit();
		
		$result = $mysqli->query($query);
		$mysqli->dbError($result);
		//$result->free_result();

		//$mysqli->close();
		//header('Location: questionform.php');
		//exit();

	} else {
		
		$result = $mysqli->query("SELECT * FROM RESPOSTAS WHERE Turma = $turma AND Numero = $number");
		$mysqli->dbError($result);
		$respostas = $result->fetch_array(MYSQLI_NUM);
		$result->free_result();

		$respostas = array_slice($respostas, 2);

		while ($question_index < $num_of_questions && $respostas[$question_index] != NULL) {
			$question_index++;
		}

	}

	$mysqli->close();
	
	if ($question_index == $num_of_questions) {
		header('Location: classification.php');
	} else {
		$_SESSION['question_numbers'] = $question_numbers;
		$_SESSION['question_index'] = $question_index;
		if(!isset($_SESSION['start_time'])) {
			$_SESSION['start_time'] = time(); 
		}
		header('Location: questionform.php');
	}

	exit();			

?>