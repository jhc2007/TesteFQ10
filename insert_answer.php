<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

include_once('setup_parameters.php');

if(isset($_POST['remainingtime']) && $_POST['remainingtime'] < 0) {
	header('Location: errormessage.php?e=2');
    exit();
}

if(isset($_SESSION['start_time'])) {
  //$start_time = $_SESSION['start_time'];
  //$now_time = time();
  if(time() - $_SESSION['start_time'] > $duration_s) {
     header('Location: errormessage.php?e=2');
     exit();
  }
}

if(isset($_SESSION['turma']) && isset($_SESSION['number']) && isset($_SESSION['question_index'])) {

	include_once('connect_db.php');
	//include_once('questions.php');
	
	$turma = $_SESSION['turma'];
	$number = $_SESSION['number'];
   	$question_index = $_SESSION['question_index'];
	$answer = $_POST['option'];

	// echo "Number:".$number."Question:".$question."Answer:".$answer;
	// exit();

	if($answer > 1)
		$answer = 0;
	// else 
	// 	$answer = 0;

	$question_index++;
    $_SESSION['question_index'] = $question_index;

	$query = "UPDATE RESPOSTAS SET R" . $question_index . " = $answer WHERE Turma = $turma AND Numero = $number;";

	// echo "Query:".$query;
	// exit();

	$result =  $mysqli->query($query);
	$mysqli->dbError($result);

	//$question_index++;
	//$_SESSION['question_index'] = $question_index;

	//$result->free_result();
	$mysqli->close();

	header('Location: questionform.php');
   	exit();

} else {
	
	header('Location: login.php');
   	exit();
}
?>