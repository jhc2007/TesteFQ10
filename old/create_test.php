<?php
   include_once('questions.php');
   include_once('header.php');

   $question = 1;
     
   while($question < sizeof($questions)) {


      echo "<p><b>" . $question . ". " . $questions[$question][0] . "</b></p>";
   
      if($questions[$question][5]) {
         echo "<img src='png/q" . $question . ".png'>";
      }
      
      echo "<form action='insert_answer.php' method='post'>";

      $options = array(1, 2, 3, 4);
      shuffle($options);

      foreach ($options as $option) {

         echo "<p>[&nbsp;&nbsp;&nbsp;] " . $questions[$question][$option] . "</p>";
      }

      // echo "<br>";

      $question++;

   } 
      
?>
</body>
</html>